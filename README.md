# Rondissimo

https://numerique53.ac-nantes.fr/ressources/applications/rondissimo.html

Rondissimo est un puzzle logique qui consiste à positionner neuf cartes sur le plateau de jeu de manière à ce que deux demi-ronds accolés aient toujours la même couleur.

Cette application s'adresse à tous les élèves, de la maternelle au CM2, à travers quarante défis de difficulté croissante. Si un défi s'avère trop difficile, une option permet à l'élève de placer jusqu'à deux cartes sur le plateau de jeu afin d'en faciliter la résolution.